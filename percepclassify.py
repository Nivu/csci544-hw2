import sys
import math
from collections import defaultdict
file_input = str(sys.argv[1])
keyword="_CLASS_"
w={}
unique_classes={}
j=0
with open(file_input,'r',errors='ignore') as f1:
 for line in f1:
  if line.startswith(keyword):
   words=line.split()
   unique_classes[j]=words[1]
   w[j]=defaultdict(int)
   j=j+1
   line=next(f1)
  words=line.split()
  w[j-1][words[0]]=words[1]
  
with open('spam_dev.txt','r') as f3:
 result={'correct':0,'wrong':0,'total':0}
 for line in f3:
  words=line.split()
  weight_class=defaultdict(int)
  for q in w:
   for word in words[1:]:
    weight_class[q]+=int(w[q][word])
  value=max(weight_class,key=weight_class.get)
 # print(value)    
  if unique_classes[value]!=words[0]:
   result['wrong']+=1
   result['total']+=1
  else:
   result['correct']+=1
   result['total']+=1
print(1-result['correct']/result['total']) 
