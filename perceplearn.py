import sys
import errno
from collections import Counter
from collections import defaultdict
file_input=str(sys.argv[1])
file_output=str(sys.argv[2])

a=0
unique_classes={}
d=open(file_output,'w',errors='ignore')
with open(file_input ,'r',errors='ignore') as f1:
 for line in f1:
  words=line.split()
  if words[0] not in unique_classes.values():
   unique_classes[a]=words[0]
   a=a+1
  else:
   continue  
  
  count={} 
#print(unique_classes)   
 #print unique words with occurences 
with open(file_input,'r',errors='ignore') as f1:
 for line in f1:
  words=line.split()
  for word in words:
   if word in count:
    count[word]+=1
   else:
    count[word]=1
 
#print(count) 

weight_class={}
average_weight={}
with open(file_input,'r',errors='ignore') as f1:
 for c in unique_classes:
  weight_class[unique_classes[c]]={}
  average_weight[unique_classes[c]]={}
  for i in count:
   weight_class[unique_classes[c]][i]=0
   average_weight[unique_classes[c]][i]=0
#print(weight_class)

class_weight={}
weight=0
for loope in range(15):
 with open(file_input,'r',errors='ignore') as f1:
  for line in f1:
   freq=defaultdict(int)
   words=line.split()
   for word in words[1:]:
    if word in freq:
     freq[word]+=1
    else:
     freq[word]=1 
    weight=0
  #print(freq)
   for i in unique_classes:
    weight=0 
    for k in freq: 
     weight+=weight_class[unique_classes[i]][k]*freq[k]  
    class_weight[unique_classes[i]]=weight
  #print(class_weight)   
   value=max(class_weight,key=class_weight.get)  
  #print(words[0])  
   if value != words[0]:
    for w in freq:
     weight_class[value][w]-=1
     weight_class[words[0]][w]+=1 
    
 for i in unique_classes:
  for w in weight_class[unique_classes[i]]:
   average_weight[unique_classes[i]][w]+=weight_class[unique_classes[i]][w] 
  
#print(weight_class)


with open(file_input,'r',errors='ignore') as f1:
# print(unique_classes)

 for i in unique_classes:
  d.write("_CLASS_ ")
  d.write(str(unique_classes[i]))
  d.write(" ")
  d.write("\n")
  for w,k in weight_class[unique_classes[i]].items():
   d.write(w)
   d.write(" ")
   d.write(str(k))
   d.write("\n")
 

 
 
