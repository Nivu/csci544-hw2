import sys
import glob
import errno
from collections import defaultdict
foutput = 'temporary.pos'
file_input=str(sys.argv[1])
file_output=str(sys.argv[2])
count={}
f2=open(foutput,'w+',errors='ignore')

with open(file_input,'r',errors='ignore') as f1:
 for line in f1:
  words=line.split()
  l=len(words)
  for i,word in enumerate(words):
   word=words[i].split('/')
   if i!=0:
    prevword=words[i-1].split('/')
   elif i==0:
    prevword=['BOS','BOS']
   if i != l-1:
    nextword=words[i+1].split('/')
   elif i==l-1:
    nextword=['EOS','EOS']
   if word[0] in count:
    count[word[0]]+=1
   else:
    count[word[0]]=1
   if prevword[0] in count:
    count[prevword[0]]+=1
   else:
    count[prevword[0]]=1
   if nextword[0] in count:
    count[nextword[0]]+=1
   else:
    count[nextword[0]]=1  
       
   f2.write(word[1]+' '+'curword:'+word[0]+' '+'prevword:'+prevword[0]+' '+'nextword:'+nextword[0]) 
   f2.write("\n")
   #print(word[1]+' '+'curword:'+word[0]+' '+'prevword:'+prevword[0]+' '+'nextword:'+nextword[0])
#print(count)
unique_classes={}  
weight_class={}
average_weight={}
a=0
with open('temporary.pos','r',errors='ignore') as f2:
 for line in f2:
  words=line.split()
  if words[0] not in unique_classes.values():
   unique_classes[a]=words[0]
   a=a+1
  else:
   continue 
#print(unique_classes)
   
with open('temporary.pos','r',errors='ignore') as f2:
 for line in f2:
  words=line.split()
  for c in unique_classes:
   weight_class[unique_classes[c]]={}
   average_weight[unique_classes[c]]={}
   for w in words[1:]:
    #print(w)
    weight_class[unique_classes[c]][w]=0 
    average_weight[unique_classes[c]][w]=0
 
  

class_weight={}

weight=0
for loope in range(10):
 with open('temporary.pos','r',errors='ignore') as f1:
  for line in f1:
   freq={}
   words=line.split()
   for word in words[1:]:
    if word in freq:
     freq[word]+=1
    else:
     freq[word]=1 
    
   #print(freq)
   #print("\n") 
   for i in unique_classes:
    weight=0 
    for k in freq: 
     if k not in weight_class[unique_classes[i]]:
      weight_class[unique_classes[i]][k]=0
     else:
      weight+=weight_class[unique_classes[i]][k]*freq[k]  
    class_weight[unique_classes[i]]=weight
   #print(class_weight)   
   value=max(class_weight,key=class_weight.get)  
  #print(words[0])  
   if value != words[0]:
    for w in freq:
     if w in weight_class[unique_classes[i]]:
      weight_class[value][w]-=1
      weight_class[words[0]][w]+=1 
    
 #for i in unique_classes:
  #for w in weight_class[unique_classes[i]]:
  # average_weight[unique_classes[i]][w]+=weight_class[unique_classes[i]][w] 
#print(weight_class)  


# print(unique_classes)

model=open(file_output,'w',errors='ignore')
for i in unique_classes:
 if(unique_classes[i] != ' '):
  model.write("_CLASS_ ")
  model.write(str(unique_classes[i]))
  model.write(" ")
  model.write("\n")
  for w,k in weight_class[unique_classes[i]].items():
   if weight_class[unique_classes[i]][w]!=0:
    model.write(w)
    model.write(" ")
    model.write(str(k))
    model.write("\n")


