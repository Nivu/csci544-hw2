import sys
import glob
import errno
from collections import defaultdict
import codecs
import itertools
finput = str(sys.argv[1])
f2=open('pos_output.txt','w',errors='ignore')
w={}
search = '_CLASS_'
label = {}
j = 0
#a=list(iter(input,'quit'))
sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
for line in sys.stdin:
 words=line.split()
 l=len(words)
 for i,word in enumerate(words):
  if i!=0:
   prevword=words[i-1]
  elif i==0:
   prevword='BOS'
  if i != l-1:
   nextword=words[i+1]
  elif i==l-1:
   nextword='EOS'
  f2.write('curword:'+str(word)+' '+'prevword:'+str(prevword)+' '+'nextword:'+str(nextword))
  f2.write('\n')
f2.close()
with open(finput, 'r') as f1:
 for line in f1:
  if line.startswith(search):
   words = line.split()
   label[j] = words[1]
   w[j] = defaultdict(int)
   j = j+1
   line = next(f1)
  words = line.split()
  w[j-1][words[0]] = words[1]
with open('pos_output.txt','r') as f3:
 for line in f3:
  words=line.split()
  currword=words[0].split(':')
  weights1=defaultdict(int)
  for key1 in w:
   for word in words:
    weights1[key1]+=int(w[key1][word])
  k = max(weights1,key=weights1.get)
  sys.stdout.write(currword[1]+'/'+label[k]+' ') 
  if words[2]=='nextword:EOS': 
   sys.stdout.write('\n') 
  
 unique_classes={}  
with open('model.pos','r') as f3:
 result={'correct':0,'wrong':0,'total':0}
 for line in f3:
  words=line.split()
  weight_class=defaultdict(int)
  for q in w:
   for word in words[1:]:
    weight_class[q]+=int(w[q][word])
   value=max(weight_class,key=weight_class.get)
  #print(value)    

   if label[value]!=words[0]:
    result['wrong']+=1
    result['total']+=1
   else:
    result['correct']+=1
    result['total']+=1
#print(1-result['correct']/result['total']) 
