a) What is the accuracy of your part of speech tagger?

Ans)  92.3451

b) What are the precision, recall and F-Score for each of the named entity types recognizer, and what is the overall F-score?

Ans)
Precision for MISC=0.64 
Recall for MISC =0.2789 
F Score for MISC= 0.412

Precision for PER=0.923
Recall for PER =0.384
F Score for PER= 0.546

Precision for LOC=0.717
Recall for LOC =0.543
F Score for LOC= 0.623

Precision for ORG=0.743
Recall for ORG=0.576
F Score for ORG= 0.652

c)
The accuracy of Naive Bayes Classifier is 75.4148% .
The accuracy is less in Naive Bayes classifier is less because it uses only bag of words feature where each word is independent and dos not depend on the words surrounding the current word and uses the Bayes theorem formula where each we compute the probability of each independent class whereas perceptron makes use of weight of each class and considers interdependencies.